from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import unittest


class NewVisitorTest(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)

    def tearDown(self):
        self.browser.quit()

    def test_can_start_a_list_and_retrieve_it_later(self):
        # Edith has heard about a cool new online to-do app. She goes
        # to check out its homepage
        self.browser.get('http://localhost:8000')
        # She notices the page title and header mention to-do lists
        self.assertIn('Cooking-Diary', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Cooking-Diary', header_text)
        # She is invited to enter a food name straight away
        inputbox = self.browser.find_element_by_id('food_name')
        self.assertEqual(
                inputbox.get_attribute('placeholder'),
                'Enter a food names'
        )
        # She types "noodle" into a text box (Edith's hobby
        # is tying fly-fishing lures)
        inputbox.send_keys('noodle')

        # When she hits enter, the page updates, and now the page lists
        # "1: noodle" as an item in a Cooking_Diary table
        inputbox.send_keys(Keys.ENTER)

        table = self.browser.find_element_by_id('food_name_table')
        rows = table.find_elements_by_tag_name('tr')
        self.assertTrue(
            any(row.text == '1: noodle' for row in rows),
            "New food_name did not appear in table"
        )
        # She is invited to enter a food name straight away
        inputbox = self.browser.find_element_by_id('comment')
        self.assertEqual(
                inputbox.get_attribute('placeholder'),
                'Enter a comment'
        )
        # She types "Good" into a text box (Edith's hobby
        # is tying fly-fishing lures)
        inputbox.send_keys('Good')

        # When she hits enter, the page updates, and now the page lists
        # "1: Good" as an item in a Cooking_Diary table
        inputbox.send_keys(Keys.ENTER)

        table = self.browser.find_element_by_id('comment_table')
        rows = table.find_elements_by_tag_name('tr')
        self.assertTrue(
            any(row.text == '1: Good' for row in rows),
            "New comment did not appear in table"
        )

        self.fail('Finish the test!')

if __name__ == '__main__':
    unittest.main(warnings='ignore')
